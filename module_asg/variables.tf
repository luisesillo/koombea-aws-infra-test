variable "asg_name" {}
variable "environment" {}
variable "lc_name" {}
variable "asg_instance_type" {}
variable "public_subnets" {
  type = "list"
}
variable "tags" {
  type = "map"
  description = "Tags [ MAP ]"
}

variable "vpc_id" {}
