data "aws_ami" "ec2-opt" {
  filter {
    name   = "name"
    values = ["amzn-ami-*-amazon-ecs-optimized"]
  }
  most_recent = true
  owners      = ["amazon"]
}

resource "aws_security_group" "asg_sg" {
  name               = "asg-ecs-luis"
  description        = "asg-ecs-luis"
  vpc_id             = "${var.vpc_id}"

  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  
#   tags               = "${merge(var.tags, map("Name", "${var.environment}_sg"))}"
}

module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 2.0"
  
  name = "${var.asg_name}"

  # Launch configuration
  lc_name = "${var.lc_name}"

  image_id        = "${data.aws_ami.ec2-opt.id}"
  instance_type   = "${var.asg_instance_type}"
  security_groups = ["${aws_security_group.asg_sg.id}"] 

  ebs_block_device = [
    {
      device_name           = "/dev/xvdz"
      volume_type           = "gp2"
      volume_size           = "50"
      delete_on_termination = true
    },
  ]

  root_block_device = [
    {
      volume_size = "50"
      volume_type = "gp2"
    },
  ]

  # Auto scaling group
  asg_name                  = "${var.asg_name}"
  key_name                  = "micros-kp"
  vpc_zone_identifier       = ["${var.public_subnets}"]
  iam_instance_profile      = "ecsInstanceRole"
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 1
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  user_data                 = <<USERDATA
#!/bin/bash
echo ECS_CLUSTER="koombea-luis-cluster" | sudo tee -a /etc/ecs/ecs.config
USERDATA

#   tags = "${merge(var.tags, map("Name", "${var.asg_name}_asg"))}"

  
}

