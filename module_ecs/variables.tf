variable "ecs_cluster_name" {}
variable "sns_arn" {}
variable "environment" {}
variable "repo_url" {}
variable "ecs_repo_name" {}
variable "aws_region" {}
variable "network_mode" {
    description = "Network type"
    default = "bridge"
}

variable memory_hard {
  description = "amount of memory limit for the container"
  default = 256
}

variable memory_soft {
  description = "amount of memory reservation for the container"
  default = 128
}

variable "iam_role" {}
variable "target_group_arn" {}

