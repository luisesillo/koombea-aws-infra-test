#ECS CLUSTER
resource "aws_ecs_cluster" "koombea-ecs" {
  name = "${var.ecs_cluster_name}"
}

data "template_file" "koombea-task-tpl" {
  template = "${file("${path.module}/../templates/ecs-task-def.json.tpl")}"

  vars = {
    environment     = "${var.environment}"
    sns_arn         = "${var.sns_arn}"
    repo_url        = "${var.repo_url}"
    ecs_repo_name   = "${var.ecs_repo_name}"
    aws_region      = "${var.aws_region}"
    memory_soft     = "${var.memory_soft}"
    memory_hard     = "${var.memory_hard}"
   }
}

resource "aws_ecs_task_definition" "default" {
  family                   = "ms-webaapp"
  container_definitions    = "${data.template_file.koombea-task-tpl.rendered}"
  network_mode             = "${var.network_mode}"
  requires_compatibilities = ["EC2"]
  volume {
    name      = "volume-0"
    host_path = "/var/run/docker.sock"
  }

}

resource "aws_ecs_service" "default" {
  name                = "ms-webapp-luis"
  cluster             = "${aws_ecs_cluster.koombea-ecs.id}"
  task_definition     = "${aws_ecs_task_definition.default.arn}"
  desired_count       = 1
  iam_role            = "${var.iam_role}"
#   depends_on          = "${var.iam_role_policy}"

  load_balancer {
    target_group_arn = "${var.target_group_arn}"
    container_name   = "${var.ecs_repo_name}"
    container_port   = 80
  }

  lifecycle {
    create_before_destroy = true
  }
}