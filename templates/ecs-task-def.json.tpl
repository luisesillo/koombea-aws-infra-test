[
  {
    "name": "${ecs_repo_name}",
    "image": "${repo_url}",
    "memory": ${memory_hard},
    "memoryReservation": ${memory_soft},
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 8080
      }
    ],
    "mountPoints": [
      {
        "readOnly": false,
        "containerPath": "/var/run/docker.sock",
        "sourceVolume": "volume-0"
      }
    ],
    "environment": [
        {
          "name": "AWS_REGION",
          "value": "${aws_region}"
        },
        {
          "name": "SNS_TOPIC",
          "value": "${sns_arn}"
        },
        {
          "name": "TAGS",
          "value": "environment:${environment},type:microservices"
        }
      ],
    "compatibilities": [
    "EC2"
  ]
  }
]
