aws_region    = "us-east-1"
aws_profile   = "koombea"
environment   = "koombea-luis"
bucket_name   = "luis-s3"
bucket_prefix = "alb-logs"

vpc_name = "koombea-luis"
vpc_cidr = "10.60.0.0/16"

availability_zones {
  "a" = "us-east-1a"
  "b" = "us-east-1b"
  "c" = "us-east-1c"
}

private_subnets = [
  "10.60.0.0/24",
  "10.60.1.0/24",
  "10.60.2.0/24",
]

# NAT GATEWAY
public_subnets = [
  "10.60.10.0/24",
  "10.60.20.0/24"
]

#ECS
ecs_cluster_name = "koombea-luis-cluster"

