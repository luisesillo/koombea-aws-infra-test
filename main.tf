data "aws_caller_identity" "current" {}

module "vpc" {
  source                         = "./module_vpc"
  name                           = "${var.vpc_name}"
  cidr                           = "${var.vpc_cidr}"
  azs                            = ["${lookup(var.availability_zones, "a")}", "${lookup(var.availability_zones, "b")}", "${lookup(var.availability_zones, "c")}"]
  private_subnets                = ["${var.private_subnets}"]
  public_subnets                 = ["${var.public_subnets}"]

  environment                    = "${var.environment}"

  tags                           = "${
    map(
     "Name", "${var.environment}-vpc",
     "Environment", "${var.environment}",
    )
  }"

}

module "ecs" {
  source                         = "./module_ecs"
  ecs_cluster_name               = "${var.ecs_cluster_name}"
  sns_arn                        = "${module.sns.sns_arn}"
  environment                    = "${var.environment}"
  repo_url                       = "${module.ecr.repository_url}"
  ecs_repo_name                  = "${var.ecs_repo_name}"
  aws_region                     = "${var.aws_region}"
  target_group_arn               = "${module.alb.target_group_arns}"
  iam_role                       = "${aws_iam_role.koombea_role.arn}"
}

module "alb" {
  source                         = "./module_alb"
  vpc_id                         = "${module.vpc.vpc_id}"
  alb_name                       = "${var.alb_name}"
  alb_type                       = "${var.alb_type}"
  alb_internal                   = "${var.alb_internal}"
  tg_name                        = "${var.tg_name}" 
  bucket_name                    = "${var.bucket_name}"
  bucket_prefix                  = "${var.bucket_prefix}"
  public_subnets                 = ["${module.vpc.public_subnets}"]
  environment                    = "${var.environment}"
  tags                           = "${
    map(
     "Name", "${var.environment}-alb",
     "Environment", "${var.environment}",
    )
  }"
}

module "sns" {
  source        = "./module_sns"
  display_name  = "Alerts: SNS - PHP -> ${var.ecs_cluster_name}"
  email_addresses = ["luis.ayazoh@gmail.com"]
  sns_topic_name    = "koombea-luis-topic"
}
module "asg" {
  source                         = "./module_asg"
  asg_name                       = "${var.asg_name}"
  vpc_id                         = "${module.vpc.vpc_id}"
  lc_name                        = "${var.lc_name}"
  asg_instance_type              = "${var.asg_instance_type}"
  # asg_security_groups           = ["${var.asg_security_groups}"]
  public_subnets                 = ["${module.vpc.public_subnets}"]
  environment                    = "${var.environment}"
  tags                           = "${
    map(
     "Name", "${var.environment}",
     "Environment", "${var.environment}",
    )
  }"
}

module "ecr" {
  source                         = "./module_ecr"
  ecs_repo_name                  = "${var.ecs_repo_name}"
}

module "ec2" {
  source                         = "./module_ec2"
  vpc_id                         = "${module.vpc.vpc_id}"
  ec2_instance_type              = "${var.ec2_instance_type}"
  # ami_id                         = "ami-0b84afb18c43907ba"
  public_subnets                 = "${ element(module.vpc.public_subnets, 0 )}"
  environment                    = "${var.environment}"
  tags                           = "${
    map(
     "Name", "jenkin-${var.environment}",
     "Environment", "${var.environment}",
    )
  }"
}

resource "aws_ssm_parameter" "secret" {
  name                           = "/micro/${var.environment}/webapp"
  description                    = "SNS TOPIC ARN"
  type                           = "String"
  value                          = "${module.sns.sns_arn}"
  tags                           = {
    environment = "${var.environment}"
  }
}

resource "aws_s3_bucket" "koombea-bucket" {
  bucket                          = "${var.bucket_name}"
  acl                             = "private"
  versioning {
    enabled                       = true
  }
  tags                            = "${
    map(
     "Name", "${var.environment}",
     "Environment", "${var.environment}",
    )
  }"

}
