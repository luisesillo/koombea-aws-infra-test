// variable "ami_id" {}
variable "environment" {}
variable "ec2_instance_type" {}
variable "public_subnets" {}
variable "tags" {
  type = "map"
  description = "Tags [ MAP ]"
}
variable "vpc_id" {}