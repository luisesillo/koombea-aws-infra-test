locals {
  instance-userdata = <<USERDATA
#!/bin/bash
sudo apt-get update -y
sudo apt-get install -y ansible
USERDATA
}

resource "aws_security_group" "ec2_sg" {
  name               = "ec2-ecs-luis"
  description        = "ec2-ecs-luis"
  vpc_id             = "${var.vpc_id}"

  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  
#   tags               = "${merge(var.tags, map("Name", "${var.environment}_sg"))}"
}
resource "aws_instance" "jenkins" {
  ami                         = "ami-04b9e92b5572fa0d1"
  instance_type               = "${var.ec2_instance_type}"
  subnet_id                   = "${var.public_subnets}"
  vpc_security_group_ids      = ["${aws_security_group.ec2_sg.id}"]
  user_data_base64            = "${base64encode(local.instance-userdata)}"
  key_name                    = "micros-kp"
  tags                        = "${var.tags}"
}
