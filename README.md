# Koombea AWS Infra Test
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This repository generates all the necessary infra to run microservices with an automatic pipeline executed by a github webhook by pushing on the repo of the web application.

### Requirements

The below requirements are needed on the host that executes this module./

  - Terraform v0.11.12
  - ansible 2.8

### AWS architecture
....
### AWS Services:

  - VPC (subnets, routes, Internet Gateway, Nat Gateway, Associations)
  - ECS
  - EC2
  - ALB and Target Group
  - SSM (system manager)
  - SNS
  - ASG (Launch configuration)
  - IAM
### Run Terraform 
```bash
$ git clone git@github.com:LuisAyazo/koombea-aws-infra-test.git
$ cd koombea-aws-infra-test
$ terraform init
$ terraform plan
$ terraform apply
```
### Run Ansible 
After running the terraform, we continue with the provisioning of the jenkins instance.
```bash
$ cd ansible 
$ ansible-playbook jenkins.yml -v
```
This playbook install all the necessary packages to be able to execute successful deployments on aws.

The packages that are installed are:

  - ansible
  - botocore
  - boto3
  - awscli
  - java - JDK 
  - pip
  - docker
  - Jenkins
 
### Jenkins Job
plugins installed:

  - Amazon ECR 
  - AnsiColor
  - Github 
  
Create a pipeline job, paste the following and update the static values.
```bash
node('master') {
  def app

  stage('Checkout'){
    git 'git@github.com:LuisAyazo/webapp-example.git'
  }

  stage('Docker build'){
      ansiColor('xterm') {
        app = docker.build('php-luis')
      }
  }

  stage('Docker push'){
    docker.withRegistry('https://0000000000.dkr.ecr.us-east-1.amazonaws.com', 'ecr:us-east-1:ecr-credentials') {
      app.push("${env.BUILD_NUMBER}")
      app.push("latest")
    }
  }
  stage('update ecs service'){
    ansiColor('xterm') {
       ansiblePlaybook colorized: true, playbook: '/var/lib/jenkins/ansible/ecs.yml', extras: '-e tag="${BUILD_NUMBER}" -v'
    }
  }

}
```

#### Activate github webhook
Steps to build a webhook in GitHub:

1. Go to your project repository.
2. Go to "settings" in the right corner.
3. Click on "webhooks."
4. Click "Add webhooks."
5. Write the Payload URL as
   http(s)://Jenkins-url/github-webhook/

> **Note:** It is necessary to add the **public ssh key** to the repository and accept it in the terminal..



