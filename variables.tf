# VPC
variable "aws_region" {}

variable "aws_profile" {}

variable "vpc_name" {}

variable "vpc_cidr" {}

variable "environment" {}

variable "private_subnets" {
  type = "list"
}

variable "public_subnets" {
  type = "list"
}

variable "availability_zones" {
  type = "map"
}

# ECS
variable "ecs_cluster_name" {}

# S3 - ALB - TFSTATE
variable "bucket_name" {}
variable "bucket_prefix" {}

# ALB
variable "alb_name" { default = "koombea-luis-alb" }
variable "alb_type" { default = "application" }
variable "alb_internal" { default = "false" }
variable "tg_name" { default = "koombea-luis-tg" }

# ECR
variable "ecs_repo_name" { default = "php-luis" }

# ASG
variable "asg_name" { default = "asg-micros" }
variable "lc_name" { default = "lc-micros" }
variable "asg_instance_type" { default = "t2.micro" }

# EC2
variable "ec2_instance_type" { default = "t2.micro" } 