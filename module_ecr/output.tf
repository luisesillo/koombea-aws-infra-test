output "registry_id" {
  description = "The account ID of the registry holding the repository."
  value = "${aws_ecr_repository.koombea-micros.registry_id}"
}
output "repository_url" {
  description = "The URL of the repository."
  value = "${aws_ecr_repository.koombea-micros.repository_url}"
}
