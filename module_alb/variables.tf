# ALB
variable "alb_name" {}
variable "alb_type" {}
variable "alb_internal" {}
variable "vpc_id" {}
variable "tags" {
  type = "map"
  description = "Tags [ MAP ]"
}

variable "bucket_name" {}
variable "bucket_prefix" {}
variable "public_subnets" {
  type = "list"
}

variable "tg_name" {}

variable "environment" {}
