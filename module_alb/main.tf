resource "aws_lb" "koombea-alb" {
  name               = "${var.alb_name}"
  internal           = "${var.alb_internal}"
  load_balancer_type = "${var.alb_type}"
  security_groups    = ["${aws_security_group.alb_sg.id}"]
  subnets            = ["${var.public_subnets}"]

  enable_deletion_protection = false
  tags               = "${var.tags}"
}

resource "aws_lb_target_group" "koombea-tg" {
  name               = "${var.tg_name}"
  port               = 8080
  protocol           = "HTTP"
  vpc_id             = "${var.vpc_id}"
  target_type        = "instance"
}

resource "aws_lb_listener" "koombea-listener" {
  load_balancer_arn = "${aws_lb.koombea-alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.koombea-tg.arn}"
  }
}

resource "aws_security_group" "alb_sg" {
  name               = "alb-ecs-luis"
  description        = "alb-ecs-luis"
  vpc_id             = "${var.vpc_id}"

  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  
  tags               = "${merge(var.tags, map("Name", "${var.environment}_sg"))}"
}