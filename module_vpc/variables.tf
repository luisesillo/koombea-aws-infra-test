variable "name" {}

variable "tags" {
  type = "map"
  description = "Tags [ MAP ]"
}

variable "environment" {}

variable "cidr" {}

variable "azs" {
  type = "list"
}

variable "private_subnets" {
  type = "list"
}

variable "public_subnets" {
  type = "list"
}

