module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "1.40.0"

  name = "${lower(var.name)}"
  cidr = "${var.cidr}"

  azs                                = ["${var.azs}"]
  private_subnets                    = ["${var.private_subnets}"]
  public_subnets                     = ["${var.public_subnets}"]

  enable_nat_gateway                 = true
  enable_vpn_gateway                 = false
  single_nat_gateway                 = true
  enable_dns_hostnames               = true
  default_vpc_enable_dns_support     = true

  tags = "${merge(var.tags, map("Name", "${var.name}_vpc"))}"

}
