provider "aws" {
  region     = "${var.aws_region}"
  version    = "~> 2.3.0"
  profile    = "${var.aws_profile}"
}

